__author__ = 'jugpreet'

import sys
import glob

def main(argv):

    directory = "data/test/"
    outputDirectory = "data/processedTestData/"
    fileNames = glob.glob(directory+"*.csv")

    for file in fileNames:
        inputFile = open(file, "r")
        outputFile = open(outputDirectory+file, "w")
        lineCount=1
        for line in inputFile:
            tempLine = ""
            if lineCount==1:
                outputFile.write(line)
            else:
                tempLine="UNK"+line
                outputFile.write(tempLine)
            lineCount = lineCount+1

if __name__ == '__main__':main(sys.argv[1:])