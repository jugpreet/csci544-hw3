__author__ = 'jugpreet'


import sys
import glob
from collections import namedtuple
from hw3_corpus_tool import get_utterances_from_file, get_utterances_from_filename, _dict_to_dialog_utterance, DialogUtterance



def main(argv):
    #directory = argv[0]
    #fileNames = glob.glob(directory+"/*.csv")
    #outputFile = open("intermediateTestFile.txt", "w")
    runDevCode = 1
    file = argv[0]
    #for file in fileNames:
    if runDevCode == 1:
        dialogUtterences = get_utterances_from_filename(file)
        lineCount = 0
        line = ""
        prevSpeaker = ""
        for dialogUtterence in dialogUtterences:
            lineCount = lineCount+1
            line = ""
            line+=dialogUtterence.act_tag+"\t"

            if lineCount==1:
                line+="firstUtterence\t"
            #else:
                #line+="\t"

            if dialogUtterence.speaker != prevSpeaker:
                line+="speakerChanged\t"
            #else:
                #line+="\t"

            prevSpeaker = dialogUtterence.speaker

            token=""
            pos=""
            if dialogUtterence.pos:
                for posValues in dialogUtterence.pos:
                    if posValues.token != "" and posValues.token != ",":
                        token+=posValues.token+"\t"
                    if posValues.pos != "" and posValues.pos != ",":
                        pos+=posValues.pos+"\t"

            if token != "":
                line+=token
            if pos != "":
                line+=pos
            #outputFile.write(line)
            #outputFile.write("\n")
            print(line, end="\n")
        print("\n")
        #outputFile.write("\n")

if __name__ == '__main__': main(sys.argv[1:])
